'''
                                  ESP Health
                         Notifiable Diseases Framework
                           Tuberculosis Case Generator


@author: Chaim Kirby <ckirby@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2012 Commonwealth Informatics
@license: LGPL
'''
from datetime import date, timedelta

from django.db import IntegrityError
from django.db.models import Max

from ESP.conf.models import LabTestMap
from ESP.hef.base import DiagnosisHeuristic, Dx_CodeQuery, LabResultPositiveHeuristic
from ESP.hef.base import LabResultAnyHeuristic, PrescriptionHeuristic
from ESP.hef.models import Event
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import CaseActiveHistory
from ESP.static.models import DrugSynonym
from ESP.emr.models import Encounter
from ESP.utils import log
from ESP.settings import DATABASES

from tb_lookups import *
from tb_utils import extract_pks, get_unbound_events, name_set, ignore_events_prior_to_case_date, \
    build_patient_event_dict, populate_patient_analysis, reclassify_patient


class Tuberculosis(DiseaseDefinition):
    '''
    Tuberculosis
    '''

    conditions = ['tuberculosis']

    legacy_uris = ['urn:x-esphealth:disease:commoninf:tuberculosis:v1']
    uri = 'urn:x-esphealth:disease:commoninf:tuberculosis:v2'

    short_name = 'tuberculosis'

    test_name_search_strings = ['tuber', 'afb', 'cult', 'mycob', 'tb', 'igra', 'feron', 'qft', 't-spot']

    timespan_heuristics = []

    def _event_name(self, name, base_name=False):
        name = name.split(':')[1]
        if base_name:
            name = name.replace('tb_', '')
        return name

    @property
    def event_heuristics(self):
        heuristic_list = []
        #
        # Diagnosis Codes
        # 010.00-018.99
        heuristic_list.append(DiagnosisHeuristic(
            name=self._event_name(HEF_NAMES['TB']),
            dx_code_queries=[
                Dx_CodeQuery(starts_with='010.', type='icd9'),
                Dx_CodeQuery(starts_with='011.', type='icd9'),
                Dx_CodeQuery(starts_with='012.', type='icd9'),
                Dx_CodeQuery(starts_with='013.', type='icd9'),
                Dx_CodeQuery(starts_with='014.', type='icd9'),
                Dx_CodeQuery(starts_with='015.', type='icd9'),
                Dx_CodeQuery(starts_with='016.', type='icd9'),
                Dx_CodeQuery(starts_with='017.', type='icd9'),
                Dx_CodeQuery(starts_with='018.', type='icd9'),
                Dx_CodeQuery(starts_with='A15.', type='icd10'),
                Dx_CodeQuery(starts_with='A16.', type='icd10'),
                Dx_CodeQuery(starts_with='A17.', type='icd10'),
                Dx_CodeQuery(starts_with='A18.', type='icd10'),
                Dx_CodeQuery(starts_with='A19.', type='icd10'),
            ]
        ))

        heuristic_list.append(DiagnosisHeuristic(
            name=self._event_name(HEF_NAMES['LTBI']),
            dx_code_queries=[
                Dx_CodeQuery(starts_with='795.5', type='icd9'),
                Dx_CodeQuery(starts_with='R76.1', type='icd10'),
                Dx_CodeQuery(starts_with='Z22.7', type='icd10'),
            ]
        ))

        for name in HEF_NAMES['AFB']:
            heuristic_list.append(LabResultPositiveHeuristic(
                test_name=self._event_name(name),
                date_field='date',
            ))

        heuristic_list.append(LabResultPositiveHeuristic(
            test_name=self._event_name(HEF_NAMES['IGRA']),
            date_field='date',
        ))

        #
        # Prescriptions
        #
        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['PYRA']),
            drugs=DrugSynonym.generics_plus_synonyms(['pyrazinamide', 'PZA', ]),
            exclude=['CAPZA', 'XTAMPZA', 'PYRANTEL'],
        ))

        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['ISON']),
            drugs=DrugSynonym.generics_plus_synonyms(['Isoniazid']),
            exclude=['INHAL', 'INHL', 'INHIB', 'rifampin', 'pyrazin', 'isonarif', 'rifamata'],
        ))

        for name in HEF_NAMES['MEDS']:
            heuristic_list.append(PrescriptionHeuristic(
                name=self._event_name(name),
                drugs=DrugSynonym.generics_plus_synonyms([self._event_name(name, base_name=True)]),
            ))

        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['RIFA']),
            drugs=DrugSynonym.generics_plus_synonyms([self._event_name(HEF_NAMES['RIFA'], base_name=True)]),
        ))

        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['ISON-MULTI']),
            drugs=['Rifamate', 'isonaRif'],
        ))

        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['ISON-MULTI-GENERIC']),
            drugs=['Isoniazid'],
            require=['rifampin'],
            exclude = ['rifamate', 'isonarif', 'pyrazin'],
        ))

        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['RFMP']),
            drugs=DrugSynonym.generics_plus_synonyms(['Rifampin']),
            exclude=['pyrazin', 'isoniazid'],
        ))

        heuristic_list.append(PrescriptionHeuristic(
            name=self._event_name(HEF_NAMES['MOXI']),
            drugs=DrugSynonym.generics_plus_synonyms(['moxifloxacin']),
            exclude=['EYE', 'OPHT', 'OPTH', 'OP SOLN'],
        ))

        return heuristic_list

    def _update_event_dict_for_existing_cases(self, events):
        patients_with_existing_cases = self.add_events_to_existing_cases(events.keys(),
                                                                         [extract_pks(events)])

        pt_ids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        for pid in pt_ids_for_existing_cases:
            events.pop(pid, None)

    def _process_case(self, patient):
        has_case = patient.find_case()
        if has_case is not None:
            trigger_event = Event.objects.get(pk=has_case[2].trigger['pk'])
            try:
                if has_case[0] not in STATUS:
                    raise ValueError()
                log.debug('Good Case for %s' % patient.patient)
                t, new_case = self._create_case_from_event_list(
                    condition=self.conditions[0],
                    criteria=has_case[1],
                    recurrence_interval=None,  # Does not recur
                    event_obj=trigger_event,
                    relevant_event_names=set([e['pk'] for e in patient.events]) - {has_case[2].trigger['pk']},
                )
                CaseActiveHistory.create(
                    case=new_case,
                    status=has_case[0],
                    date=new_case.date,
                    change_reason="TB-I",
                    event_rec=trigger_event,
                )
                log.info('Created new %s Tuberculosis case %s : %s' % (has_case[0], has_case[1], new_case))
                return 1
            except IntegrityError:
                log.debug('Case creation failed for     %s' % patient.patient)
                t = False
            except ValueError:
                log.debug('Case creation failed for     %s. Bad status %s' % patient.patient, has_case[0])
                t = False

        return 0

    def _most_recent_history_by_case(self):
        db_engine = DATABASES['default']['ENGINE']
        if db_engine == 'django.db.backends.postgresql_psycopg2': # postgresql
            return CaseActiveHistory.objects.filter(
                                                status__in=['TB-L', 'TB-A']
                                            ).order_by(
                                                'case__pk', '-date'
                                            ).distinct('case__pk').select_related()
        else: # other backend
            raw_q = ("SELECT * FROM nodis_caseactivehistory a "
                     "INNER JOIN "
                     "(SELECT max(date) date, case_id "
                     "FROM nodis_caseactivehistory "
                     "WHERE status in ('TB-L', 'TB-A') "
                     "GROUP BY case_id) b "
                     "ON a.case_id=b.case_id AND a.date=b.date")
            return CaseActiveHistory.objects.raw(raw_q)

    def _reclassify_tb(self):
        most_recent = {'TB-L': [], 'TB-A': []}

        for recent_case_history in self._most_recent_history_by_case():
            most_recent.get(recent_case_history.status).append(recent_case_history)
        reclassified_latent = self._reclassify_latent_tb(most_recent['TB-L'])
        reclassified_active = self._reclassify_active_tb(most_recent['TB-A'])
        if reclassified_latent + reclassified_active > 0:
            log.info('Reclassified %s cases for %s (%s)' % (
                reclassified_latent + reclassified_active, self.short_name, self.uri))
            self._reclassify_tb()

    def _reclassify_latent_tb(self, case_active_list):
        reclassified_count = 0
        if len(case_active_list) == 0:
            return reclassified_count
        for case_active in case_active_list:
            if case_active.case.events.last().date <= case_active.latest_event_date:
                continue
            reclassified = reclassify_patient('latent', case_active.case.events.filter(
                date__gt=case_active.latest_event_date)).find_case()
            if reclassified is not None:
                trigger_event = Event.objects.get(pk=reclassified[2].trigger['pk'])
                case_status = 'RQ' if case_active.case.status in ['S', 'RS'] else case_active.case.status
                reclassified_count += self._update_case(case_active.case,
                                                        reclassified[1],
                                                        case_status,
                                                        "TB-A",
                                                        trigger_event,
                                                        trigger_event.date,
                                                        "TB-LA")
            else:
                case_active.latest_event_date = case_active.case.events.last().date
                case_active.save()

        return reclassified_count

    def _reclassify_active_tb(self, case_active_list):
        reclassified_count = 0
        if len(case_active_list) == 0:
            return reclassified_count
        for case_active in case_active_list:
            if (case_active.latest_event_date + timedelta(days=365)) >= date.today():
                continue
            reclassified_count += self._update_case(case_active.case,
                                                    'Criteria Active to Latent: Revert to latent after 365 days',
                                                    case_active.case.status,
                                                    "TB-L",
                                                    case_active.case.events.last(),
                                                    case_active.date + timedelta(days=365),
                                                    "TB-AL")

        return reclassified_count

    def _update_case(self, case, criteria, case_status, status, event, change_date, change_reason):
        case.criteria = criteria
        case.status = case_status
        status = status
        event = Event.objects.get(pk=event.pk)
        change_date = change_date
        change = change_reason
        if change_reason == "TB-AL":
            event.date = change_date

        case.save()
        return CaseActiveHistory.create(
            case=case,
            status=status,
            date=change_date,
            change_reason=change,
            event_rec=event,
        )

    def generate_tb(self):
        unbound_events = get_unbound_events(name_set(), self.conditions[0])
        new_or_no_case_events = ignore_events_prior_to_case_date(unbound_events, self.conditions[0])
        patient_events = build_patient_event_dict(new_or_no_case_events)
        self._update_event_dict_for_existing_cases(patient_events)
        new_cases = 0
        for patient in populate_patient_analysis(patient_events):
            new_cases += self._process_case(patient)

        return new_cases

    def generate(self):
        LabTestMap.create_update_dummy_lab(self.conditions[0], 'MDPH-250', 'MDPH-R348')
        log.info('Generating cases of %s' % self.short_name)
        counter = self.generate_tb()
        log.debug('Generated %s new cases of tuberculosis' % counter)
        log.info('Reclassifying cases for %s (%s)' % (self.short_name, self.uri))
        self._reclassify_tb()

        return counter  # Count of new cases

    def report_field(self, report_field, case):
        reportable_fields = {
            'na_trmt_obx': False,
            'symptom_obx': False,
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
            'skin_test_result': self._get_skin_test_result(case),
            'disease': 'NA-1731',
            'disease_status': self._get_disease_status(case),
        }

        return reportable_fields.get(report_field, None)

    def _get_skin_test_result(self, case):
        if case.events.filter(name__in=['dx:tb_ltbi']).count() > 0:
            evnts=case.events.filter(name__in=['dx:tb_ltbi'])
            for obj in evnts:
                enc=Encounter.objects.get(id=obj.object_id)
                if enc.dx_codes_str.__contains__('icd10:R76.11') or enc.dx_codes_str.__contains__('icd9:795.51'):
                    return True
        return False

    def _get_disease_status(self, case):
        status = {'TB-A': 'YES', 'TB-L': 'NO'}
        return ('', status.get(case.caseactivehistory_set.last().status, None))


# -------------------------------------------------------------------------------
#
# Packaging
#
# -------------------------------------------------------------------------------

tuberculosis_definition = Tuberculosis()


def event_heuristics():
    return tuberculosis_definition.event_heuristics


def disease_definitions():
    return [tuberculosis_definition]
