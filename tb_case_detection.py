import abc
import copy
from collections import OrderedDict, defaultdict
from datetime import date
from operator import itemgetter

from tb_lookups import HEF_NAMES
from tb_utils import build_event_date_offset_map, name_set, events_offset


class EventsNotFoundError(Exception):
    pass


class MissingTriggerError(Exception):
    pass


class MissingVerifyingError(Exception):
    pass


class AnalysisBase(object):
    def __init__(self, events, event_date_offset_map, first_event, criteria, **kwargs):
        self.trigger = None
        self.case_events = []
        self.negating_event = None
        self.first_event = first_event
        self.events = events
        self.criteria = criteria
        self.event_date_offset_map = event_date_offset_map
        self.params = kwargs

    def get_events_by_offset_range(self, start=0, end=None, inclusive=False):
        events = []
        if end is None:
            end = max(self.event_date_offset_map.keys()) + 1
        elif inclusive:
            end += 1

        for offset in range(start, end):
            events.extend(self.event_date_offset_map.get(offset, []))
        if events:
            return events
        else:
            raise EventsNotFoundError

    def get_events_by_names(self, names, event_set=None):
        events = defaultdict(list)
        for event in event_set or self.events:
            if event['name'] in names:
                events[event['name']].append(event)

        if events:
            return events
        else:
            raise EventsNotFoundError

    def get_first_events_by_names(self, names, event_set=None):
        return sorted([vals[0] for key, vals in self.get_events_by_names(names, event_set).items()],
                      key=itemgetter('date'))

    def get_events_after_trigger(self):
        if self.trigger is not None:
            case_offset = events_offset(self.trigger, self.first_event)
            try:
                for name, events in self.get_events_by_names(name_set(),
                                                             self.get_events_by_offset_range(
                                                                 start=case_offset + 1)).items():
                    self.add_events(events)
            except EventsNotFoundError:
                pass

            return self.case_events


class Analysis(AnalysisBase):
    __metaclass__ = abc.ABCMeta

    def run(self):
        self.analyze()
        self.get_events_after_trigger()

    @abc.abstractmethod
    def analyze(self, **kwargs):
        pass


class MultiEventAnalysis(Analysis):
    def __init__(self, *args, **kwargs):
        super(MultiEventAnalysis, self).__init__(*args, **kwargs)
        self.trigger_names = kwargs['trigger_names']
        self.validate_names = kwargs['validate_names']
        self.before = kwargs['offset_before']
        self.after = kwargs['offset_after']

    def analyze(self):
        self.find_trigger()
        events = self.find_validation()
        self.validate(events)

    def add_events(self, events):
        self.case_events.extend(events)

    def find_trigger(self):
        try:
            events = self.get_first_events_by_names(self.trigger_names)
            self.trigger = events[0]
        except EventsNotFoundError:
            raise MissingTriggerError

    def find_validation(self):
        offset = events_offset(self.trigger, self.first_event)
        try:
            range_events = self.get_events_by_offset_range(start=offset - self.before, end=offset + self.after,
                                                           inclusive=True)
            events = self.get_events_by_names(self.validate_names, event_set=range_events)
        except EventsNotFoundError:
            raise MissingVerifyingError

        return events

    def validate(self, events):
        pass


class NegateMultiEventAnalysis(MultiEventAnalysis):
    def validate(self, events):
        names = set(events.keys())
        if len(names) < 2:
            raise MissingVerifyingError
        if len(names - set(name_set(['ISON', 'RIFA']))) == 0:
            raise MissingVerifyingError


class SingleEventAnalysis(Analysis):
    def analyze(self):
        try:
            events = self.get_first_events_by_names(self.params['event_names'])
            self.trigger = events[0]
        except EventsNotFoundError:
            raise MissingTriggerError

    def add_events(self, events):
        self.case_events.extend(events)


class PatientAnalysis(object):
    def __init__(self, events, current_criteria=None):
        self.events = sorted(events, key=itemgetter('date'))
        self.criteria = self._build_criteria(current_criteria)
        self.patient = self._get_events_patient()
        self.first_event = self.events[0]
        self.event_date_offset_map = build_event_date_offset_map(self.first_event, self.events)
        self.now = date.today()

    def _get_events_patient(self):
        patient_list = set([e['patient'] for e in self.events])
        if not len(patient_list) == 1:
            raise ValueError(
                "PatientAnalysis instance got events for {} patients {}".format(len(patient_list), patient_list))
        return list(patient_list)[0]

    def _build_criteria(self, current_criteria=None):
        potential = OrderedDict()
        potential['active_1'] = self.active_1_eligible
        potential['active_2'] = self.active_2_eligible
        potential['active_3'] = self.active_3_eligible
        potential['latent_1'] = self.latent_1_eligible
        potential['latent_2'] = self.latent_2_eligible
        potential['latent_3'] = self.latent_3_eligible
        potential['latent_4'] = self.latent_4_eligible

        if current_criteria is not None:
            for name in [key for key in potential.keys() if current_criteria in key]:
                del potential[name]
        return potential

    def find_case(self):
        criteria_copy = copy.deepcopy(self.criteria)
        for key, event_method in criteria_copy.items():
            try:
                analysis = event_method()
                analysis.run()
                if analysis.trigger is not None:
                    self.criteria[key] = analysis
                else:
                    del self.criteria[key]
            except (MissingTriggerError, MissingVerifyingError):
                del self.criteria[key]

        return self.select_case_event_type([(key, analysis) for key, analysis in self.criteria.items()])

    def select_case_event_type(self, criteria_event):
        if not criteria_event:
            return None

        eligible = sorted(criteria_event, key=lambda x: x[1].trigger['date'])[0]

        name, analysis = eligible[0], eligible[1]
        criteria = 'Criteria {}: {}'.format(name.replace('_', ' ').capitalize(), analysis.criteria)
        status = "TB-L" if "latent" in name else "TB-A"

        return (status, criteria, analysis)

    def active_1_eligible(self):
        return SingleEventAnalysis(self.events,
                                   self.event_date_offset_map,
                                   self.first_event,
                                   "Pyrazinamide Rx",
                                   event_names=[HEF_NAMES['PYRA']])

    def active_2_eligible(self):
        return MultiEventAnalysis(self.events,
                                  self.event_date_offset_map,
                                  self.first_event,
                                  "AFB and TB dx -14 to +60 days",
                                  trigger_names=name_set(['AFB']),
                                  validate_names=[HEF_NAMES['TB']],
                                  offset_before=14,
                                  offset_after=60)

    def active_3_eligible(self):
        return NegateMultiEventAnalysis(self.events,
                                        self.event_date_offset_map,
                                        self.first_event,
                                        "TB dx and 2 TB Rx -60 to + 60 days",
                                        trigger_names=[HEF_NAMES['TB']],
                                        validate_names=name_set(
                                            ['MEDS', 'PYRA', 'ISON', 'RIFA', 'ISON-MULTI', 'ISON-MULTI-GENERIC']),
                                        offset_before=60,
                                        offset_after=60)


    def latent_1_eligible(self):
        return SingleEventAnalysis(self.events,
                                   self.event_date_offset_map,
                                   self.first_event,
                                   "LTBI dx",
                                   event_names=[HEF_NAMES['LTBI']])

    def latent_2_eligible(self):
        return SingleEventAnalysis(self.events,
                                   self.event_date_offset_map,
                                   self.first_event,
                                   "Positive IGRA",
                                   event_names=[HEF_NAMES['IGRA']])

    def latent_3_eligible(self):
        return SingleEventAnalysis(self.events,
                                   self.event_date_offset_map,
                                   self.first_event,
                                   "Isoniazid Rx",
                                   event_names=[HEF_NAMES['ISON']])

    def latent_4_eligible(self):
        return SingleEventAnalysis(self.events,
                                   self.event_date_offset_map,
                                   self.first_event,
                                   "Rifapentine Rx",
                                   event_names=[HEF_NAMES['RIFA']])
