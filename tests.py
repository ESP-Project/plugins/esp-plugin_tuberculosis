import unittest
from datetime import datetime, date, timedelta

from tb_case_detection import EventsNotFoundError, AnalysisBase, MissingVerifyingError, MissingTriggerError


class MockEvent(object):
    def __init__(self, name=None, patient=10, date=None):
        import random
        import string
        self.pk = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
        self.date = date
        self.name = name
        self.patient = patient

    def __getitem__(self, item):
        return self.__dict__[item]


class TestTB(unittest.TestCase):
    def setUp(self):
        self.events = [
            MockEvent(name='lx', patient='1', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='rx', patient='1', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-03', '%Y-%m-%d')),
            MockEvent(name='lx', patient='2', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='rx', patient='2', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='dx', patient='2', date=datetime.strptime('2000-01-03', '%Y-%m-%d')),
        ]

        self.multiple_events_by_date = [
            MockEvent(name='lx', patient='1', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='rx', patient='1', date=datetime.strptime('2000-01-03', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-05', '%Y-%m-%d')),
        ]

        self.dx_tb = MockEvent(name='dx:tb_tb', patient='1',
                               date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.dx_ltbi = MockEvent(name='dx:tb_ltbi', patient='1',
                                 date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.lx_afb = MockEvent(name='lx:tb_afb:positive', patient='1',
                                date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.lx_igra = MockEvent(name='lx:tb_igra:positive', patient='1',
                                 date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_pyra = MockEvent(name='rx:tb_pyrazinamide', patient='1',
                                 date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_ison = MockEvent(name='rx:tb_isoniazid', patient='1',
                                 date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_rifa = MockEvent(name='rx:tb_rifapentine', patient='1',
                                 date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_ison_multi = MockEvent(name='rx:tb_isonizaid-rifampin', patient='1',
                                       date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_ison_multi_generic = MockEvent(name='rx:tb_isonizaid-rifampin_generic', patient='1',
                                               date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_1 = MockEvent(name='rx:tb_moxifloxacin', patient='1',
                              date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_2 = MockEvent(name='rx:tb_rifampin', patient='1',
                              date=datetime.strptime('1999-01-01', '%Y-%m-%d'))
        self.rx_3 = MockEvent(name='rx:tb_para_aminosalicyclic_acid', patient='1',
                              date=datetime.strptime('1999-01-01', '%Y-%m-%d'))

    def tearDown(self):
        pass

    def _get_analysis_from_pa(self, pa, klass=None):
        first_event = pa.first_event
        events = pa.events
        event_offset_map = pa.event_date_offset_map

        if klass is None:
            klass = AnalysisBase

        return klass(events, event_offset_map, first_event, "Test Criteria")

    def _run_analysis(self, analysis):
        analysis.run()
        return analysis

    def test_build_patient_event_dict(self):
        from tb_utils import build_patient_event_dict
        patient_dict = build_patient_event_dict(self.events)
        expected = {'1': self.events[0:3],
                    '2': self.events[3:6]}

        self.assertDictEqual(patient_dict, expected)

    def test_patient_analysis_init(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis(self.events[0:3])
        self.assertEqual(pa.now, date.today())
        self.assertEqual(pa.patient, '1')
        self.assertEqual(pa.first_event, self.events[1])
        self.assertListEqual(pa.events, [self.events[1], self.events[0], self.events[2]])
        offset_dict = {1: [self.events[0]], 0: [self.events[1]], 2: [self.events[2]]}
        self.assertDictEqual(pa.event_date_offset_map, offset_dict)

    def test_mixed_patient_analysis_error(self):
        from tb_case_detection import PatientAnalysis
        with self.assertRaises(ValueError):
            PatientAnalysis(self.events[0:4])

    def test_build_event_date_offset_map(self):
        from tb_case_detection import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)
        offset_dict = {0: [evs[0], evs[2]], 1: [evs[3], evs[4]], 2: [evs[1]], 4: [evs[5]]}
        self.assertDictEqual(pa.event_date_offset_map, offset_dict)

    def test_events_offset(self):
        from tb_utils import events_offset
        self.assertEqual(events_offset(self.events[1], self.events[2]), 2)
        self.assertEqual(events_offset(self.events[0], self.events[1]), 1)

    def test_events_offset_by_range(self):
        from tb_case_detection import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)

        analysis = self._get_analysis_from_pa(pa)
        self.assertItemsEqual(analysis.get_events_by_offset_range(0, 3), [evs[0], evs[1], evs[2], evs[3], evs[4]])
        self.assertItemsEqual(analysis.get_events_by_offset_range(-22, 3), [evs[0], evs[1], evs[2], evs[3], evs[4]])
        with self.assertRaises(EventsNotFoundError):
            analysis.get_events_by_offset_range(3, 4)

    def test_get_events_by_name(self):
        from tb_case_detection import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)
        analysis = self._get_analysis_from_pa(pa)
        self.assertDictEqual(analysis.get_events_by_names(['lx', 'rx', 'dx']),
                             {'lx': [evs[0]], 'rx': [evs[1]], 'dx': evs[2:]})
        self.assertDictEqual(analysis.get_events_by_names(['dx']), {'dx': evs[2:]})
        self.assertDictEqual(analysis.get_events_by_names(['dx'], analysis.get_events_by_offset_range(0, 3)),
                             {'dx': evs[2:5]})
        with self.assertRaises(EventsNotFoundError):
            analysis.get_events_by_names(['fail'])
        with self.assertRaises(EventsNotFoundError):
            analysis.get_events_by_names(['lx'], analysis.get_events_by_offset_range(1, 3))

    def test_get_first_events_by_names(self):
        from tb_case_detection import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)
        analysis = self._get_analysis_from_pa(pa)
        self.assertItemsEqual(analysis.get_first_events_by_names(['lx', 'rx', 'dx']), [evs[0], evs[1], evs[2]])
        self.assertItemsEqual(analysis.get_first_events_by_names(['lx', 'dx']), [evs[0], evs[2]])
        self.assertItemsEqual(
            analysis.get_first_events_by_names(['lx', 'dx'], analysis.get_events_by_offset_range(4, 6)), [evs[5]])

    def test_latents_eligible(self):
        from tb_case_detection import PatientAnalysis
        method_events = [(PatientAnalysis.latent_1_eligible, self.dx_ltbi),
                         (PatientAnalysis.latent_2_eligible, self.lx_igra),
                         (PatientAnalysis.latent_3_eligible, self.rx_ison),
                         (PatientAnalysis.latent_4_eligible, self.rx_rifa)]
        for me in method_events:
            pa = PatientAnalysis([me[1]])
            self.assertEqual(self._run_analysis(me[0](pa)).trigger, me[1])

    def test_latents_ineligible(self):
        from tb_case_detection import PatientAnalysis
        method_events = [(PatientAnalysis.latent_4_eligible, self.dx_ltbi),
                         (PatientAnalysis.latent_1_eligible, self.lx_igra),
                         (PatientAnalysis.latent_2_eligible, self.rx_ison),
                         (PatientAnalysis.latent_3_eligible, self.rx_rifa)]
        for me in method_events:
            pa = PatientAnalysis([me[1]])
            with self.assertRaises(MissingTriggerError):
                me[0](pa).run()

    def test_active_1_eligible(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.rx_pyra])
        self.assertEqual(self._run_analysis(pa.active_1_eligible()).trigger, self.rx_pyra)

    def test_active_1_no_trigger(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.lx_afb])
        with self.assertRaises(MissingTriggerError):
            pa.active_1_eligible().run()

    def test_active_2_eligible(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.lx_afb, self.dx_tb])
        self.assertEqual(self._run_analysis(pa.active_2_eligible()).trigger, self.lx_afb)

    def test_active_2_no_trigger(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.dx_tb])
        with self.assertRaises(MissingTriggerError):
            pa.active_2_eligible().run()

    def test_active_2_dx_before_eligible(self):
        from tb_case_detection import PatientAnalysis
        self.dx_tb.date = self.lx_afb.date - timedelta(days=14)
        pa = PatientAnalysis([self.lx_afb, self.dx_tb])
        self.assertEqual(self._run_analysis(pa.active_2_eligible()).trigger, self.lx_afb)

    def test_active_2_dx_after_eligible(self):
        from tb_case_detection import PatientAnalysis
        self.dx_tb.date = self.lx_afb.date + timedelta(days=60)
        pa = PatientAnalysis([self.lx_afb, self.dx_tb])
        self.assertEqual(self._run_analysis(pa.active_2_eligible()).trigger, self.lx_afb)

    def test_active_2_dx_before_ineligible(self):
        from tb_case_detection import PatientAnalysis
        self.dx_tb.date = self.lx_afb.date - timedelta(days=15)
        pa = PatientAnalysis([self.lx_afb, self.dx_tb])
        with self.assertRaises(MissingVerifyingError):
            pa.active_2_eligible().run()

    def test_active_2_dx_after_ineligible(self):
        from tb_case_detection import PatientAnalysis
        self.dx_tb.date = self.lx_afb.date + timedelta(days=61)
        pa = PatientAnalysis([self.lx_afb, self.dx_tb])
        with self.assertRaises(MissingVerifyingError):
            pa.active_2_eligible().run()

    def test_active_3_eligible(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.dx_tb, self.rx_1, self.rx_2])
        self.assertEqual(self._run_analysis(pa.active_3_eligible()).trigger, self.dx_tb)

    def test_active_3_no_trigger(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.rx_1, self.rx_2])
        with self.assertRaises(MissingTriggerError):
            pa.active_3_eligible().run()

    def test_active_3_eligible_with_offset_rx(self):
        from tb_case_detection import PatientAnalysis
        self.rx_1.date -= timedelta(days=60)
        self.rx_2.date += timedelta(days=60)
        pa = PatientAnalysis([self.dx_tb, self.rx_1, self.rx_2])
        self.assertEqual(self._run_analysis(pa.active_3_eligible()).trigger, self.dx_tb)

    def test_active_3_ineligible_with_offset_rx(self):
        from tb_case_detection import PatientAnalysis
        self.rx_1.date -= timedelta(days=61)
        self.rx_2.date += timedelta(days=60)
        pa = PatientAnalysis([self.dx_tb, self.rx_1, self.rx_2])
        with self.assertRaises(MissingVerifyingError):
            pa.active_3_eligible().run()

    def test_active_3_ineligible_duplicate_drug(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.dx_tb, self.rx_1, self.rx_1])
        with self.assertRaises(MissingVerifyingError):
            pa.active_3_eligible().run()

    def test_active_3_ineligible_inh_rpt(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.dx_tb, self.rx_ison, self.rx_rifa])
        with self.assertRaises(MissingVerifyingError):
            pa.active_3_eligible().run()

    def test_active_3_eligible_inh_rpt_and_one(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.dx_tb, self.rx_ison, self.rx_rifa, self.rx_1])
        self.assertIsNotNone(self._run_analysis(pa.active_3_eligible()).trigger)

    def test_find_case(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.rx_1])
        self.assertIsNone(pa.find_case())

        pa = PatientAnalysis([self.lx_igra])
        self.assertEquals(pa.find_case()[:-1], ("TB-L", "Criteria Latent 2: Positive IGRA"))

    def test_acute_preference(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.rx_ison, self.rx_pyra])
        self.assertEquals(pa.find_case()[:-1], ("TB-A", "Criteria Active 1: Pyrazinamide Rx"))

    def test_earliest_preference(self):
        from tb_case_detection import PatientAnalysis
        self.rx_ison.date -= timedelta(days=3)
        pa = PatientAnalysis([self.rx_ison, self.rx_pyra])
        self.assertEquals(pa.find_case()[:-1], ("TB-L", "Criteria Latent 3: Isoniazid Rx"))

    def test_find_case_pk(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.lx_igra])
        self.assertEqual(pa.find_case()[2].trigger['pk'], self.lx_igra['pk'])

    def test_build_criteria_for_reclassify(self):
        from tb_case_detection import PatientAnalysis
        pa = PatientAnalysis([self.lx_igra], 'latent')
        self.assertEqual(len(pa.criteria), 4)
        pa = PatientAnalysis([self.lx_igra], 'active')
        self.assertEqual(len(pa.criteria), 4)

    def test_correct_non_reclassify(self):
        from tb_case_detection import PatientAnalysis
        self.rx_1.date -= timedelta(days=60)
        self.rx_2.date += timedelta(days=60)
        pa = PatientAnalysis([self.dx_tb, self.rx_1, self.rx_2], current_criteria='active')
        self.assertIsNone(pa.find_case())
