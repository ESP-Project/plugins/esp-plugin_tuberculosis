HEF_NAMES = {'TB': 'dx:tb_tb',
             'LTBI': 'dx:tb_ltbi',
             'AFB': ['lx:tb_afb:positive',
                     'lx:tb_afb:negative',
                     'lx:tb_afb:indeterminate'],
             'IGRA': 'lx:tb_igra:positive',
             'PYRA': 'rx:tb_pyrazinamide',
             'ISON': 'rx:tb_isoniazid',
             'MEDS': ['rx:tb_ethambutol',
                      'rx:tb_rifabutin',
                      'rx:tb_streptomycin',
                      'rx:tb_para_aminosalicyclic_acid',
                      'rx:tb_kanamycin',
                      'rx:tb_capreomycin',
                      'rx:tb_cycloserine',
                      'rx:tb_ethionamide'],
             'RFMP': 'rx:tb_rifampin',      
             'MOXI': 'rx:tb_moxifloxacin',
             'RIFA': 'rx:tb_rifapentine',
             'ISON-MULTI': 'rx:tb_isoniazid-rifampin',
             'ISON-MULTI-GENERIC': 'rx:tb_isoniazid-rifampin_generic'
             }

STATUS = {"TB-A": "Acute", "TB-L": "Latent"}
CHANGE_REASON = {"TB-I": "Initial", "TB-LA": "Latent to Active", "TB-AL": "Active to Latent"}
