from collections import defaultdict


def name_set(keys=None):
    from tb_lookups import HEF_NAMES
    names = []
    if keys is None:
        keys = HEF_NAMES.keys()
    for x, y in HEF_NAMES.items():
        if x in keys:
            if isinstance(y, list):
                names.extend(y)
            else:
                names.append(y)
    return names


def get_events(lookup, values, order='date'):
    from ESP.hef.models import Event
    return Event.objects.filter(**lookup).order_by(order).values(*values)


def get_unbound_events(names, condition):
    return [e for e in get_events({'name__in': names},
                                  ['pk', 'name', 'patient', 'date', 'case__condition'])
            if not e['case__condition'] == condition]


def build_patient_event_dict(events):
    patient_events = defaultdict(list)
    for ev in events:
        patient_events[ev['patient']].append(ev)

    return patient_events


def populate_patient_analysis(patient_events):
    from tb_case_detection import PatientAnalysis
    patient_analysis = []
    for key, elems in patient_events.items():
        patient_analysis.append(PatientAnalysis(elems))
    return patient_analysis


def ignore_events_prior_to_case_date(events, condition):
    from ESP.nodis.models import Case
    patient_case_dates = {c['patient']: c['date'] for c in
                          Case.objects.filter(condition=condition).values('patient', 'date')}
    return [e for e in events if e['date'] >= patient_case_dates.get(e['patient'], e['date'])]


def build_event_date_offset_map(offset_from, events):
    ed_map = defaultdict(list)
    for event in events:
        ed_map[events_offset(event, offset_from)].append(event)
    return ed_map


def extract_pks(patient_event_dict):
    return {patient: [event['pk'] for event in events] for patient, events in patient_event_dict.items()}


def events_offset(one, two):
    return abs((one['date'] - two['date']).days)


def reclassify_patient(criteria, events):
    from tb_case_detection import PatientAnalysis
    return PatientAnalysis(
        events.values('pk', 'name', 'patient', 'date', 'case__condition'),
        current_criteria=criteria)

